from copy import copy
from os import listxattr
        # exemples de matchs de foot
match1=('2021-06-28', 'France', 'Switzerland', 3, 3, 'UEFA Euro', 'Bucharest', 'Romania', True)
match2=('1998-07-12', 'France', 'Brazil', 3, 0, 'FIFA World Cup', 'Saint-Denis', 'France', False)
match3=('1978-04-05', 'Germany', 'Brazil', 0, 1, 'Friendly', 'Hamburg', 'Germany', False)

#exemples de liste de matchs de foot
liste1=[('1970-04-08', 'France', 'Bulgaria', 1, 1, 'Friendly', 'Rouen', 'France', False), 
        ('1970-04-28', 'France', 'Romania', 2, 0, 'Friendly', 'Reims', 'France', False), 
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False), 
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
liste2=[('1901-03-09', 'England', 'Northern Ireland', 3, 0, 'British Championship', 'Southampton', 'England', False), 
        ('1901-03-18', 'England', 'Wales', 6, 0, 'British Championship', 'Newcastle', 'England', False), 
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False), 
        ('1902-05-03', 'England', 'Scotland', 2, 2, 'British Championship', 'Birmingham', 'England', False), 
        ('1903-02-14', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Wolverhampton', 'England', False), 
        ('1903-03-02', 'England', 'Wales', 2, 1, 'British Championship', 'Portsmouth', 'England', False), 
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False), 
        ('1905-02-25', 'England', 'Northern Ireland', 1, 1, 'British Championship', 'Middlesbrough', 'England', False), 
        ('1905-03-27', 'England', 'Wales', 3, 1, 'British Championship', 'Liverpool', 'England', False), 
        ('1905-04-01', 'England', 'Scotland', 1, 0, 'British Championship', 'London', 'England', False), 
        ('1907-02-16', 'England', 'Northern Ireland', 1, 0, 'British Championship', 'Liverpool', 'England', False), 
        ('1907-03-18', 'England', 'Wales', 1, 1, 'British Championship', 'London', 'England', False), 
        ('1907-04-06', 'England', 'Scotland', 1, 1, 'British Championship', 'Newcastle', 'England', False), 
        ('1909-02-13', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Bradford', 'England', False), 
        ('1909-03-15', 'England', 'Wales', 2, 0, 'British Championship', 'Nottingham', 'England', False), 
        ('1909-04-03', 'England', 'Scotland', 2, 0, 'British Championship', 'London', 'England', False)
        ]
liste3=[('1901-03-30', 'Belgium', 'France', 1, 2, 'Friendly', 'Bruxelles', 'Belgium', False),
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False),
        ('1903-04-04', 'Brazil', 'Argentina', 3, 0, 'Friendly', 'Sao Paulo', 'Brazil', False),
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False), 
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False), 
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
liste4=[('1978-03-19', 'Argentina', 'Peru', 2, 1, 'Copa Ramón Castilla', 'Buenos Aires', 'Argentina', False), 
        ('1978-03-29', 'Argentina', 'Bulgaria', 3, 1, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-04-05', 'Argentina', 'Romania', 2, 0, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-05-03', 'Argentina', 'Uruguay', 3, 0, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-01', 'Germany', 'Poland', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-02', 'Argentina', 'Hungary', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-02', 'France', 'Italy', 1, 2, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-02', 'Mexico', 'Tunisia', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-03', 'Austria', 'Spain', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-03', 'Brazil', 'Sweden', 1, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-03', 'Iran', 'Netherlands', 0, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-03', 'Peru', 'Scotland', 3, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-06', 'Argentina', 'France', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-06', 'Germany', 'Mexico', 6, 0, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-06', 'Hungary', 'Italy', 1, 3, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-06', 'Poland', 'Tunisia', 1, 0, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-07', 'Austria', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-07', 'Brazil', 'Spain', 0, 0, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-07', 'Iran', 'Scotland', 1, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-07', 'Netherlands', 'Peru', 0, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-10', 'Argentina', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-10', 'France', 'Hungary', 3, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-10', 'Germany', 'Poland', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-11', 'Austria', 'Brazil', 0, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-11', 'Iran', 'Peru', 1, 4, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-11', 'Netherlands', 'Scotland', 2, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-11', 'Spain', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-14', 'Argentina', 'Poland', 2, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-14', 'Austria', 'Netherlands', 1, 5, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-14', 'Brazil', 'Peru', 3, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-14', 'Germany', 'Italy', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-18', 'Argentina', 'Brazil', 0, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-18', 'Austria', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-18', 'Germany', 'Netherlands', 2, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-18', 'Peru', 'Poland', 0, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-21', 'Argentina', 'Peru', 6, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-21', 'Austria', 'Germany', 3, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-21', 'Brazil', 'Poland', 3, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-21', 'Italy', 'Netherlands', 1, 2, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-24', 'Brazil', 'Italy', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-25', 'Argentina', 'Netherlands', 3, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False)
]

liste_fus = [('1901-03-09', 'England', 'Northern Ireland', 3, 0, 'British Championship', 'Southampton', 'England', False), 
        ('1901-03-18', 'England', 'Wales', 6, 0, 'British Championship', 'Newcastle', 'England', False), 
        ('1901-03-30', 'Belgium', 'France', 1, 2, 'Friendly', 'Bruxelles', 'Belgium', False),
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False), 
        ('1902-05-03', 'England', 'Scotland', 2, 2, 'British Championship', 'Birmingham', 'England', False), 
        ('1903-02-14', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Wolverhampton', 'England', False), 
        ('1903-03-02', 'England', 'Wales', 2, 1, 'British Championship', 'Portsmouth', 'England', False), 
        ('1903-04-04', 'Brazil', 'Argentina', 3, 0, 'Friendly', 'Sao Paulo', 'Brazil', False),
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False), 
        ('1905-02-25', 'England', 'Northern Ireland', 1, 1, 'British Championship', 'Middlesbrough', 'England', False), 
        ('1905-03-27', 'England', 'Wales', 3, 1, 'British Championship', 'Liverpool', 'England', False), 
        ('1905-04-01', 'England', 'Scotland', 1, 0, 'British Championship', 'London', 'England', False), 
        ('1907-02-16', 'England', 'Northern Ireland', 1, 0, 'British Championship', 'Liverpool', 'England', False), 
        ('1907-03-18', 'England', 'Wales', 1, 1, 'British Championship', 'London', 'England', False), 
        ('1907-04-06', 'England', 'Scotland', 1, 1, 'British Championship', 'Newcastle', 'England', False), 
        ('1909-02-13', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Bradford', 'England', False), 
        ('1909-03-15', 'England', 'Wales', 2, 0, 'British Championship', 'Nottingham', 'England', False), 
        ('1909-04-03', 'England', 'Scotland', 2, 0, 'British Championship', 'London', 'England', False),
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False), 
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
# -----------------------------------------------------------------------------------------------------
# listes des fonctions à implémenter
# -----------------------------------------------------------------------------------------------------

# Fonctions à implémenter dont les tests sont fournis


def equipe_gagnante(match):
    """retourne le nom de l'équipe qui a gagné le match. Si c'est un match nul on retourne None

    Args:
        match (tuple): un match

    Returns:
        str: le nom de l'équipe gagnante (ou None si match nul)
    """    
    res=0
    if match[3]>match[4]:
        res=match[1]
    elif match[3]<match[4]:
        res=match[2]
    elif match[3]==match[4]:
        res=None
    
    return res


def victoire_a_domicile(match):
    """indique si le match correspond à une victoire à domicile 

    Args:
        match (tuple): un match

    Returns:
        bool: True si le match ne se déroule pas en terrain neutre et que l'équipe qui reçoit a gagné
    """    
    if match[8]==False and match[3]>match[4] :  
        return True
    else :
        return False


def nb_buts_marques(match):
    """indique le nombre total de buts marqués lors de ce match

    Args:
        match (tuple): un match

    Returns:
        int: le nombre de buts du match 
    """    
    return (match[3]+match[4])


def matchs_ville(liste_matchs, ville):
    """retourne la liste des matchs qui se sont déroulés dans une ville donnée
    
    Args:
        liste_matchs (list): une liste de matchs
        ville (str): le nom d'une ville

    Returns:
        list: la liste des matchs qui se sont déroulé dans la ville ville    
    """
    res=[]
    for match in liste_matchs : 
        if match[6]==ville :
            res.append(match)
    return res

def nombre_moyen_buts(liste_matchs, nom_competition):
    """retourne le nombre moyen de buts marqués par match pour une compétition donnée

    Args:
        liste_matchs (list): une liste de matchs
        nom_competition (str): le nom d'une compétition
    
    Returns:
        float: le nombre moyen de buts par match pour la compétition
    """
    res=0
    x=0
    for match in liste_matchs:
        if match[5]==nom_competition :
            res+=match[3]+match[4]
            x+=1
    return res/x

def est_bien_trie(liste_matchs):
    """vérifie si une liste de matchs est bien trié dans l'ordre chronologique
       puis pour les matchs se déroulant le même jour, dans l'ordre alphabétique
       des équipes locales

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        bool: True si la liste est bien triée et False sinon
    """    
    for i in range(1,len(liste_matchs)) : 
        if liste_matchs[i][0][0:4]<liste_matchs[i-1][0][0:4]:
            return False
        if liste_matchs[i][0][0:4]==liste_matchs[i-1][0][0:4] and liste_matchs[i][0][5:7]<liste_matchs[i-1][0][5:7] :
            return False 
        if liste_matchs[i][0][0:4]==liste_matchs[i-1][0][0:4] and liste_matchs[i][0][5:7]==liste_matchs[i-1][0][5:7] and liste_matchs[i][0][8:10]<liste_matchs[i-1][0][8:10]:
            return False
        if liste_matchs[i][0][0:4]==liste_matchs[i-1][0][0:4] and liste_matchs[i][0][5:7]==liste_matchs[i-1][0][5:7] and liste_matchs[i][0][8:10]==liste_matchs[i-1][0][8:10] and liste_matchs[i][1]<liste_matchs[i-1][1]:
            return False
    return True 

def fusionner_matchs(liste_matchs1, liste_matchs2):
    """Fusionne deux listes de matchs triées sans doublons en une liste triée sans doublon
    sachant qu'un même match peut être présent dans les deux listes

    Args:
        liste_matchs1 (list): la première liste de matchs
        liste_matchs2 (list): la seconde liste de matchs

    Returns:
        list: la liste triée sans doublon comportant tous les matchs de liste_matchs1 et liste_matchs2
    """ 
    a=len(liste_matchs1)
    b=len(liste_matchs2)
    
    if a>b :
        c=copy(liste_matchs1)
        for i in range(len(liste_matchs2)):
            for y in range(len(c)+1):
                if liste_matchs2[i] not in c :
                    c.insert(y,liste_matchs2[i])
                    if not(est_bien_trie(c)) :
                        del c[y]  
        return c
    
    if a<b :
        c=copy(liste_matchs2)
        for i in range(len(liste_matchs1)):
            for y in range(len(c)+1):
                if liste_matchs1[i] not in c :
                    c.insert(y,liste_matchs1[i])
                    if not(est_bien_trie(c)) :
                        del c[y]
        return c

def resultats_equipe(liste_matchs, equipe):
    """donne le nombre de victoire, de matchs nuls et de défaites pour une équipe donnée

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        tuple: un triplet d'entiers contenant le nombre de victoires, nuls et défaites de l'équipe
    """    
    defaite_equipe=0
    victoire_equipe=0
    nul_equipe=0
    
    for match in liste_matchs :
        if match[1]==equipe:
            if match[3]>match[4]:
                victoire_equipe+=1
            if match[3]<match[4]:
                defaite_equipe+=1
            if match[3]==match[4]:
                nul_equipe+=1
        
        if match[2]==equipe:
            if match[3]<match[4]:
                victoire_equipe+=1
            if match[3]>match[4]:
                defaite_equipe+=1
            if match[3]==match[4]:
                nul_equipe+=1
    return ((victoire_equipe,nul_equipe,defaite_equipe))

def plus_gros_scores(liste_matchs):
    """retourne la liste des matchs pour lesquels l'écart de buts entre le vainqueur et le perdant est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs avec le plus grand écart entre vainqueur et perdant
    """    
    res=[]
    plus_gros_score=0
    for match in liste_matchs :
        if abs(match[3]-match[4]) > plus_gros_score :
            plus_gros_score=abs(match[3]-match[4])
    for matchs in liste_matchs:
        if abs(matchs[3]-matchs[4]) == plus_gros_score:
            res.append(matchs)
    return res

def liste_des_equipes(liste_matchs):
    """retourne la liste des équipes qui ont participé aux matchs de la liste
    Attention on ne veut voir apparaitre le nom de chaque équipe qu'une seule fois

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: une liste de str contenant le noms des équipes ayant jouer des matchs
    """
    res=[]  
    for equipe in liste_matchs:
        if equipe[1] not in res :
            res.append(equipe[1])
        if equipe[2] not in res:
            res.append(equipe[2])
    return res

def premiere_victoire(liste_matchs, equipe):
    """retourne la date de la première victoire de l'equipe. Si l'equipe n'a jamais gagné de match on retourne None

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        str: la date de la première victoire de l'equipe
    """    
    for victory in liste_matchs:
        if victory[1] == equipe and victory[3]>victory[4]:
            return victory[0]
        if victory[2] == equipe and victory[3]<victory[4]:
            return victory[0]

    return None

def nb_matchs_sans_defaites(liste_matchs, equipe):
    """retourne le plus grand nombre de matchs consécutifs sans défaite pour une equipe donnée.

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        int: le plus grand nombre de matchs consécutifs sans défaite du pays nom_pays
    """
    record_match_consecutif=0
    record_match_consecutif_en_cours=0
    for match in liste_matchs :
        if match[1]==equipe or match[2]==equipe:
            if equipe_gagnante(match)==equipe or equipe_gagnante(match)==None:
                record_match_consecutif_en_cours+=1
            else :
                record_match_consecutif=record_match_consecutif_en_cours
                record_match_consecutif_en_cours=0
    
    record_match_consecutif=record_match_consecutif_en_cours    
    return record_match_consecutif


def charger_matchs(nom_fichier):
    """charge un fichier de matchs donné au format CSV en une liste de matchs

    Args:
        nom_fichier (str): nom du fichier CSV contenant les matchs

    Returns:
        list: la liste des matchs du fichier
    """    
    res=[]
    e=[]
    fic=open(nom_fichier,"r")
    fic.readline()
    for ligne in fic:
        l_champs=ligne.split(",")
        e.extend((l_champs[0],l_champs[1],l_champs[2],int(l_champs[3]),int(l_champs[4]),l_champs[5],l_champs[6],l_champs[7],l_champs[8]))
        if e[8][0]=='F':
            e[8]='False'
            u=tuple(e)
            res.append(u)
            e=[]
        else : 
            e[8]='True'
            u=tuple(e)
            res.append(u)
            e=[]
    fic.close()
    return res

def sauver_matchs(liste_matchs,nom_fichier):
    """sauvegarde dans un fichier au format CSV une liste de matchs

    Args:
        liste_matchs (list): la liste des matchs à sauvegarder
        nom_fichier (str): nom du fichier CSV

    Returns:
        None: cette fonction ne retourne rien
    """    
    fic=open(nom_fichier,"w")
    fic.write("Match,type,pv\n")
    for elem in liste_matchs:
        fic.write(elem[0]+","+elem[1]+","+str(elem[2])+","+elem[3]+","+elem[4]+","+elem[5]+","+(elem[6])+","+(elem[7])+","+elem[8]+"\n")
        fic.close()

def plus_de_victoires_que_defaites(liste_matchs, equipe):
    """vérifie si une équipe donnée a obtenu plus de victoires que de défaites
    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        bool: True si l'equipe a obtenu plus de victoires que de défaites
    """
    if resultats_equipe(liste_matchs,equipe)[0]-resultats_equipe(liste_matchs,equipe)[2]>0:
        return True

    else:
        return False 


def matchs_spectaculaires(liste_matchs):
    """retourne la liste des matchs les plus spectaculaires, c'est à dire les
    matchs dont le nombre total de buts marqués est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs les plus spectaculaires
    """
    res=[]
    plus_gros_score=0
    for match in liste_matchs :
        if match[3]+match[4] > plus_gros_score :
            plus_gros_score=match[3]+match[4]
    for matchs in liste_matchs:
        if matchs[3]+matchs[4] == plus_gros_score:
            res.append(matchs)
    return res


def meilleures_equipes(liste_matchs):
    """retourne la liste des équipes de la liste qui ont le plus petit nombre de defaites

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des équipes qui ont le plus petit nombre de defaites
    """
    plus_petit_nb_defaite=[]
    a=liste_des_equipes(liste_matchs)
    for equipe in a :
        x=resultats_equipe(liste_matchs,equipe)
        plus_petit_nb_defaite.append(x[2])

    cpt=plus_petit_nb_defaite[0]
    res=[]
    for i in range(len(plus_petit_nb_defaite)):
        if plus_petit_nb_defaite[i] <= min(plus_petit_nb_defaite) :
            cpt=plus_petit_nb_defaite[i]
            res.append(a[i])
    return res


def classement (liste_match,competition):  #Fonction Bonus
    """retourne le classement des équipes pour une compétion donné selon leurs résultats (leurs points) dans cette compétion.
    Le classement sera sous forme de liste qui sera triée de la meilleur équipe à la moins bonne,
    le nombre de point et l'équipe doivent figurer dans un tuple. Rappelons que 1 Victoire=3pnt, 1 Match nul = 1 pnt
    et 1 défaite = 0pnt.
    
    Args : Une liste de matchs et une le nom d'une compétition
    
    Returns : Une liste trié contenant toute les équipes qui participent à la compétition, de la meilleur
    à la moins bonne  """

    classement=[]
    classementfinale=[]
    i=0
    p=[]
    for m in liste_match : 
        if m[5]==competition :
            p.append(m)
    equipe=liste_des_equipes(liste_match) 
    for match in p:
        eq=match[1]
        equi=match[2]
        i+=1
        if p[i-1][5]==competition :
            if (eq,resultats_equipe(p,eq)) not in classement :
                classement.append((eq,resultats_equipe(p,eq)))
            if (equi,resultats_equipe(p,equi)) not in classement :
                classement.append((equi,resultats_equipe(p,equi)))
    for elem in classement :
        classementfinale.append(((elem[1][0]*3+elem[1][1],elem[0])))

    return sorted(classementfinale, reverse = True)

def but_marques_totales(liste_matchs,nom_equipe): #Fontion Bonus
    """Cette fonction prend en paramètre une liste de matchs et une équipe, et renvoie
    le nombre de but totale qu'elle a inscrit.
    
    Args : Liste de matchs (str) et le nom d'une equipe (str)
    
    Returns : Renvoie le nombre totale de but que l'équipe donné a inscrit"""

    res=0
    for match in liste_matchs:
        if match[1]==nom_equipe:
            res+=match[3]
        if match[2]==nom_equipe:
            res+=match[4]
    return res

def classement_liste_match(liste_matchs):  #Fonction Bonus
    """Cette fonction reprend la meme structure que la fontion classement, mais 
    ne prend pas en paramètre une compétiton, elle prend seulement en parametre 
    un nom de fichier et renvoie un classement par point selon les résultats de toute 
    les équipes qui jouent
    
    Args : Nom d'un fichier
    
    Returns : Un classement comportant toute les équipes du fichier avec leur place dans 
    le classement et avec leur nombre de points (3 pnts = Victoire, 1 pnt = Match Nul et 0 pnt = Défaite)"""

    res=[]
    classement=[]
    classementfinale=[]
    i=0
    for match in liste_matchs:
        eq=match[1]
        equi=match[2]
        i+=1
        if (eq,resultats_equipe(liste_matchs,eq)) not in classement :
            classement.append((eq,resultats_equipe(liste_matchs,eq)))
        if (equi,resultats_equipe(liste_matchs,equi)) not in classement :
            classement.append((equi,resultats_equipe(liste_matchs,equi)))
    for elem in classement :
        classementfinale.append(((elem[1][0]*3+elem[1][1],elem[0])))

    x = sorted(classementfinale, reverse = True)
    for elem in range(len(x)):
        print(elem+1,"-",x[elem][1],":",x[elem][0])
    return

def nombre_moyen_buts_equipe(liste_matchs, equipe): #Fonction Bonus
    """retourne le nombre moyen de buts marqués par match pour une équipe donnée

    Args:
        liste_matchs (list): une liste de matchs
        nom_competition (str): le nom d'une équipe
    
    Returns:
        float: le nombre moyen de buts par match pour l'équipe
    """
    res=0
    x=0
    for match in liste_matchs:
        if match[1]==equipe :
            res+=match[3]
            x+=1
        if match[2]==equipe :
            res+=match[4]
            x+=1
    if x==0 :
        return
    return res/x

def nombre_but_conceder(liste_match,equipe): #Fonction Bonus
    '''Cette fonction prend en paramètre une liste de match et une équipe, et renvoie
    le nombre de but totale qu'elle a concéder.
    
    Args : Liste de matchs (str) et le nom d'une equipe (str)
    
    Returns : Renvoie le nombre totale de but que l'équipe a concéder'''

    res=0
    for match in liste_match:
        if match[1]==equipe:
            res+=match[4]
        if match[2]==equipe:
            res+=match[3]
    return res

def trier_par_date (liste_matchs,datedeb,datefin): #Fonction Bonus
    res=[]
    for match in liste_matchs :
        a=match[0].split('-')
        if int(a[0])>=datedeb and int(a[0])<=datefin :
            res.append(match)
    return res

def trier_par_competition (liste_matchs,competition): #Fonction Bonus
    res=[]
    for match in liste_matchs :
        if match[5]==competition : 
            res.append(match)
    return res

def ratio_victoire_defaite(liste_matchs,equipe): #Fonction Bonus
    if resultats_equipe(liste_matchs,equipe)[2] != 0 :
        return round(resultats_equipe(liste_matchs,equipe)[0]/resultats_equipe(liste_matchs,equipe)[2],2) 
    else : 
        return round(resultats_equipe(liste_matchs,equipe)[0],2)

def ratio_but_marquee_concedee (liste_matchs,equipe): #Fonction Bonus 
    if nombre_but_conceder(liste_matchs,equipe) != 0 :
        return round(but_marques_totales(liste_matchs,equipe)/nombre_but_conceder(liste_matchs,equipe),2)
    else :
        return round(but_marques_totales(liste_matchs,equipe),2)

def matchs_spectaculaires_equipe(liste_matchs,equipe): #Fonction Bonus
    """retourne la liste des matchs les plus spectaculaires, c'est à dire les
    matchs dont le nombre total de buts marqués est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs les plus spectaculaires
    """
    res=[]
    plus_gros_score=0
    for match in liste_matchs :
        if match[3]+match[4] > plus_gros_score and (match[1] == equipe or match[2] == equipe) :
            plus_gros_score=match[3]+match[4]
    for matchs in liste_matchs :
        if (matchs[3]+matchs[4] == plus_gros_score) and (matchs[1] == equipe or matchs[2] == equipe) :
            res.append(matchs)
            return res
