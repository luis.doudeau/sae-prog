import histoire2foot

def programme_principal():
    l=0
    m=0
    n=0
    nom_fichier=None
    while nom_fichier != "histoire1.csv" and nom_fichier != "histoire2.csv" and nom_fichier != "histoire3.csv" :
        nom_fichier=input("Rentre un nom de fichier CSV à charger : ")
    equipe = input("Quel est ton équipe nationale de foot préférée ? ")
    while equipe not in histoire2foot.liste_des_equipes(histoire2foot.charger_matchs(nom_fichier)):
        print("Cette équipe n'éxiste pas, réessaye en ecrivant ton équipe en Anglais ! Par exemple : England pour Angleterre")
        equipe=input("Quel est votre équipe nationale de foot préférée ? ")
    rep=None
    while rep != 'Y' and rep != 'N' and rep != 'Yes' and rep != 'Oui' and rep != 'oui' and rep != 'yes' and rep != 'No' and rep != 'NO' and 'YES' and rep != 'Non' and rep != 'non' :
        rep=input("Veux-tu trier les matchs selon une date ou une compétition ? (Oui/Non - Yes/No - Y/N) ")
    
    if rep=='Y' or rep=='Yes' or rep=='yes' or rep=='YES' or rep=='Oui' or rep=='oui' :
        print(
            "\n"
            "1 --> Par date"+"\n"
            "2 --> Par compétition"+"\n"
            "3 --> Par date et compétition"+"\n"
        )
        tri = None
        while tri != "1" and tri != "2" and tri !="3":
            tri=input("Entre le chiffre correspondant à ta demande (1, 2 ou 3) ")
        if tri == '1':
            l=1
            anneedeb=''
            anneefin=''
            while not(anneedeb.isdecimal) or len(anneedeb)!=4:
                anneedeb=input("Entre l'année de début (Sous cette forme : XXXX, exemple : 2015) ")
            while not(anneefin.isdecimal()) or len(anneefin)!=4 or anneedeb > anneefin :
                anneefin=input("Entre l'année de fin (Sous cette forme : XXXX, exemple : 2015) ")
            z=histoire2foot.trier_par_date((histoire2foot.charger_matchs(nom_fichier)),int(anneedeb),int(anneefin))
        elif tri == '2':
            m=1
            competition=None
            while competition!= '1' and competition!= '2' and competition!= '3' and competition!= '4' and competition!= '5' :
                print(
                    "1 --> Friendly"+"\n"
                    "2 --> British Championship"+"\n"
                    "3 --> Copa América"+"\n"
                    "4 --> UEFA Euro"+"\n"
                    "5 --> FIFA World Cup"+"\n"
                )
                competition=input("Entre le numéro correspondant à votre choix ")
            if competition == '1':
                competition = 'Friendly'
            elif competition == '2':
                competition = 'British Championship'
            elif competition == '3':
                competition = 'Copa América'
            elif competition == '4':
                competition = 'UEFA Euro'
            elif competition == '5':
                competition = 'FIFA World Cup'
            z=histoire2foot.trier_par_competition(histoire2foot.charger_matchs(nom_fichier),competition)
        elif tri == '3':
            n=1
            anneedeb=''
            anneefin=''
            while not(anneedeb.isdecimal()) or len(anneedeb)!=4:
                anneedeb=input("Entre l'année de début (Sous cette forme : XXXX, exemple : 2015) ")
            while not(anneefin.isdecimal()) or len(anneefin)!=4 or anneedeb > anneefin :
                anneefin=input("Entre l'année de fin (Sous cette forme : XXXX, exemple : 2015) ")
            competition=None
            while competition!= '1' and competition!= '2' and competition!= '3' and competition!= '4' and competition!= '5' :
                print(
                    "1 --> Friendly"+"\n"
                    "2 --> British Championship"+"\n"
                    "3 --> Copa América"+"\n"
                    "4 --> UEFA Euro"+"\n"
                    "5 --> FIFA World Cup"+"\n"
                )
                competition=input("Entre le numéro correspondant à votre choix ")
            if competition == '1':
                competition = 'Friendly'
            elif competition == '2':
                competition = 'British Championship'
            elif competition == '3':
                competition = 'Copa América'
            elif competition == '4':
                competition = 'UEFA Euro'
            elif competition == '5':
                competition = 'FIFA World Cup'

            z=histoire2foot.trier_par_date((histoire2foot.trier_par_competition(histoire2foot.charger_matchs(nom_fichier),competition)),int(anneedeb),int(anneefin))
    else  :
        z=histoire2foot.charger_matchs(nom_fichier)
    if equipe[0] in 'AEIOUY':
        if z==[] and l==1 :
            print("\n","Cette équipe n'a participé à aucun match durant les dates séléctionnées")
            return
        elif z==[] and m==1 :
            print("\n","Cette équipe n'a participé à aucun match pour la compétition séléctionnées")
            return
        elif z==[] and n==1 :
            print("\n","Cette équipe n'a participé à aucun match durant les dates et/ou pour la compétition séléctionnées")
            return
        else :
            print("\n","L'equipe d'",equipe,"a remporté",histoire2foot.resultats_equipe(z,equipe)[0],"matchs, a fait",histoire2foot.resultats_equipe(z,equipe)[1],"matchs nul et a concédé",histoire2foot.resultats_equipe(z,equipe)[2],"défaites.","\n","Elle a marqué un totale de",histoire2foot.but_marques_totales(z,equipe),"buts.","Sa première victoire remonte au",histoire2foot.premiere_victoire(z,equipe),".","\n","L'",equipe,"a un record de",histoire2foot.nb_matchs_sans_defaites(z,equipe),"matchs de suite sans défaites.","\n","Le nombre de but moyen de l'",equipe,"est de",round(histoire2foot.nombre_moyen_buts_equipe(z,equipe),2),"buts par match et cette équipe a concédée",histoire2foot.nombre_but_conceder(z,equipe),"buts.","\n","Le ratio Victoire/Défaite de l'",equipe,"est de",histoire2foot.ratio_victoire_defaite(z,equipe),"\n","Le ratio But Marqué/But Concédé de l'",equipe,"est",histoire2foot.ratio_but_marquee_concedee(z,equipe))
            print("\n","Voici le classement complet de l'équipe d'",equipe,":","\n")
        histoire2foot.classement_liste_match(z)
    else :
        if z==[] and l==1 :
            print("\n","Cette équipe n'a participé à aucun match durant les dates séléctionnées")
            return
        elif z==[] and m==1 :
            print("\n","Cette équipe n'a participé à aucun match pour la compétiton séléctionnées")
            return
        elif z==[] and n==1 :
            print("\n","Cette équipe n'a participé à aucun match durant les dates et/ou pour la compétiton séléctionnées")
            return
        else :
            print("\n","L'equipe de",equipe,"a remporté",histoire2foot.resultats_equipe(z,equipe)[0],"matchs, a fait",histoire2foot.resultats_equipe(z,equipe)[1],"matchs nul et a concédé",histoire2foot.resultats_equipe(z,equipe)[2],"défaites.","\n","Elle a marqué un totale de",histoire2foot.but_marques_totales(z,equipe),"buts.","Sa première victoire remonte au",histoire2foot.premiere_victoire(z,equipe),".","\n","La",equipe,"a un record de",histoire2foot.nb_matchs_sans_defaites(z,equipe),"matchs de suite sans défaites.","\n","Le nombre de but moyen de la",equipe,"est de",round(histoire2foot.nombre_moyen_buts_equipe(z,equipe),2),"buts par match et cette équipe a concédée",histoire2foot.nombre_but_conceder(z,equipe),"buts.","\n","Le ratio Victoire/Défaite de la",equipe,"est de",histoire2foot.ratio_victoire_defaite(z,equipe),"\n","Le ratio But Marqué/But Concédé de la",equipe,"est",histoire2foot.ratio_but_marquee_concedee(z,equipe),"et le match le plus spectaculaire de la",equipe,"est :",histoire2foot.matchs_spectaculaires_equipe(z,equipe)[0][1],":",histoire2foot.matchs_spectaculaires_equipe(z,equipe)[0][3],"-",histoire2foot.matchs_spectaculaires_equipe(z,equipe)[0][4],":",histoire2foot.matchs_spectaculaires_equipe(z,equipe)[0][2])
            print("\n","Voici le classement complet de l'équipe de",equipe,"avec les paramètres de tries choisis : (cela peut prendre plusieurs minutes...)","\n")
            histoire2foot.classement_liste_match(z)

programme_principal()
